﻿
namespace Hector
{
    partial class FenetreImporter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool Disposing)
        {
            if (Disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(Disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OpenFileDialogImporter = new System.Windows.Forms.OpenFileDialog();
            this.ProgressBarImporter = new System.Windows.Forms.ProgressBar();
            this.AjouterButton = new System.Windows.Forms.Button();
            this.EcraserButton = new System.Windows.Forms.Button();
            this.SelectionFichier = new System.Windows.Forms.Button();
            this.TextBoxFilePathImporter = new System.Windows.Forms.TextBox();
            this.BackgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.RichTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // OpenFileDialogImporter
            // 
            this.OpenFileDialogImporter.FileName = "openFileDialogImporter";
            this.OpenFileDialogImporter.Filter = "CSV files (*.csv)|*.csv";
            // 
            // ProgressBarImporter
            // 
            this.ProgressBarImporter.Location = new System.Drawing.Point(12, 415);
            this.ProgressBarImporter.Name = "ProgressBarImporter";
            this.ProgressBarImporter.Size = new System.Drawing.Size(776, 23);
            this.ProgressBarImporter.TabIndex = 1;
            // 
            // AjouterButton
            // 
            this.AjouterButton.Location = new System.Drawing.Point(185, 372);
            this.AjouterButton.Name = "AjouterButton";
            this.AjouterButton.Size = new System.Drawing.Size(75, 23);
            this.AjouterButton.TabIndex = 2;
            this.AjouterButton.Text = "Ajouter";
            this.AjouterButton.UseVisualStyleBackColor = true;
            // 
            // EcraserButton
            // 
            this.EcraserButton.Location = new System.Drawing.Point(540, 372);
            this.EcraserButton.Name = "EcraserButton";
            this.EcraserButton.Size = new System.Drawing.Size(75, 23);
            this.EcraserButton.TabIndex = 3;
            this.EcraserButton.Text = "Ecraser";
            this.EcraserButton.UseVisualStyleBackColor = true;
            this.EcraserButton.Click += new System.EventHandler(this.EcraserButton_Click);
            // 
            // SelectionFichier
            // 
            this.SelectionFichier.Location = new System.Drawing.Point(24, 17);
            this.SelectionFichier.Name = "SelectionFichier";
            this.SelectionFichier.Size = new System.Drawing.Size(96, 52);
            this.SelectionFichier.TabIndex = 4;
            this.SelectionFichier.Text = "Ouvrir fichier .csv";
            this.SelectionFichier.UseVisualStyleBackColor = true;
            this.SelectionFichier.Click += new System.EventHandler(this.SelectionFichier_Click);
            // 
            // TextBoxFilePathImporter
            // 
            this.TextBoxFilePathImporter.Location = new System.Drawing.Point(139, 32);
            this.TextBoxFilePathImporter.Name = "TextBoxFilePathImporter";
            this.TextBoxFilePathImporter.Size = new System.Drawing.Size(625, 22);
            this.TextBoxFilePathImporter.TabIndex = 5;
            // 
            // BackgroundWorker1
            // 
            this.BackgroundWorker1.WorkerReportsProgress = true;
            this.BackgroundWorker1.WorkerSupportsCancellation = true;
            this.BackgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackgroundWorker1_DoWork);
            this.BackgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BackgroundWorker1_ProgressChanged);
            this.BackgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BackgroundWorker1_RunWorkerCompleted);
            // 
            // RichTextBox1
            // 
            this.RichTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RichTextBox1.Location = new System.Drawing.Point(12, 101);
            this.RichTextBox1.Name = "RichTextBox1";
            this.RichTextBox1.ReadOnly = true;
            this.RichTextBox1.Size = new System.Drawing.Size(776, 242);
            this.RichTextBox1.TabIndex = 6;
            this.RichTextBox1.Text = "";
            // 
            // FenetreImporter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.RichTextBox1);
            this.Controls.Add(this.TextBoxFilePathImporter);
            this.Controls.Add(this.SelectionFichier);
            this.Controls.Add(this.EcraserButton);
            this.Controls.Add(this.AjouterButton);
            this.Controls.Add(this.ProgressBarImporter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FenetreImporter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Importer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog OpenFileDialogImporter;
        private System.Windows.Forms.ProgressBar ProgressBarImporter;
        private System.Windows.Forms.Button AjouterButton;
        private System.Windows.Forms.Button EcraserButton;
        private System.Windows.Forms.Button SelectionFichier;
        private System.Windows.Forms.TextBox TextBoxFilePathImporter;
        private System.ComponentModel.BackgroundWorker BackgroundWorker1;
        private System.Windows.Forms.RichTextBox RichTextBox1;
    }
}