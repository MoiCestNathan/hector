﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hector.DAO
{
    public interface IArticleDAO
    {
        void Insert(Article article);
        void Update(Article article);
        void Delete(string refArticle);
        Article GetByRefArticle(string refArticle);
        List<Article> GetAll();
    }
}
