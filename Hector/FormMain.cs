﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

/// <summary>
/// Classe principale du formulaire principal de l'application Hector.
/// </summary>
/// /// <remarks>
/// Cette classe contient les méthodes pour gérer les événements du formulaire principal.
/// </remarks>
namespace Hector
{
    public partial class FormMain : Form
    {
        private SQLiteConnection Connection;

        private new ContextMenuStrip ContextMenuStrip;

        private const int IndexColonneDescription = 1;

        private const int IndexColonneFamille = 2;

        private const int IndexColonneSousFamille = 3;

        private const int IndexColonneMarque = 4;

        /// <summary>
        /// Constructeur de la classe FormMain.
        /// </summary>
        public FormMain()
        {
            InitializeComponent();

            InitializeContextMenuStrip();

            InitializeDatabaseConnection();

            PopulateTreeView();
        }

        /// <summary>
        /// Gère l'événement de clic sur le menu "Importer".
        /// </summary>
        /// <param name="Sender">L'objet qui a déclenché l'événement.</param>
        /// <param name="Event">Les arguments de l'événement.</param>
        private void ImporterToolStripMenuItem_Click(object Sender, EventArgs Event)
        {
            var FenetreImporter = new FenetreImporter
            {
                MainForm = this
            };

            FenetreImporter.ShowDialog(this);
        }

        /// <summary>
        /// Gère l'événement de clic sur le menu "Exporter".
        /// </summary>
        /// <param name="Sender">L'objet qui a déclenché l'événement.</param>
        /// <param name="Event">Les arguments de l'événement.</param>
        private void ExporterToolStripMenuItem_Click(object Sender, EventArgs Event)
        {
            var FenetreExporter = new FenetreExporter();

            FenetreExporter.ShowDialog(this);
        }

        /// <summary>
        /// Initialise la connexion à la base de données SQLite.
        /// </summary>
        private void InitializeDatabaseConnection()
        {
            string SolutionDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;

            string DatabaseFileName = "HectorTest.SQLite";

            string AbsolutePath = Path.Combine(SolutionDirectory, "ExternalFiles", DatabaseFileName);

            string ConnectionString = $"Data Source={AbsolutePath};Version=3;Charset=utf8;";

            Connection = new SQLiteConnection(ConnectionString);

            Connection.Open();
        }

        /// <summary>
        /// Remplit l'arborescence avec les données de la base de données.
        /// </summary>
        private void PopulateTreeView()
        {
            TreeView1.Nodes.Clear();

            // Ajouter le nœud "Familles" avec toutes les descriptions d'articles
            TreeNode FamillesNode = new TreeNode("Tous les articles");

            using (SQLiteCommand Command = new SQLiteCommand("SELECT Description FROM Articles", Connection))
            {
                using (SQLiteDataReader Reader = Command.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        FamillesNode.Nodes.Add(Reader.GetString(0)); // Ajouter chaque description d'article comme un nœud sous "Familles"
                    }
                }
            }

            TreeView1.Nodes.Add(FamillesNode);

            // Ajouter le nœud "Familles" avec les familles et sous-familles associées
            TreeNode FamillesAvecSousFamillesNode = new TreeNode("Familles");

            using (SQLiteCommand Command = new SQLiteCommand("SELECT Nom FROM Familles", Connection))
            {
                using (SQLiteDataReader Reader = Command.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        string FamilleNom = Reader.GetString(0);

                        TreeNode FamilleNode = new TreeNode(FamilleNom);

                        // Récupérer les sous-familles associées à cette famille
                        using (SQLiteCommand SousFamillesCommand = new SQLiteCommand("SELECT RefSousFamille, Nom FROM SousFamilles WHERE RefFamille = (SELECT RefFamille FROM Familles WHERE Nom = @Nom)", Connection))
                        {
                            SousFamillesCommand.Parameters.AddWithValue("@Nom", FamilleNom);

                            using (SQLiteDataReader SousFamillesReader = SousFamillesCommand.ExecuteReader())
                            {
                                while (SousFamillesReader.Read())
                                {
                                    int RefSousFamille = SousFamillesReader.GetInt32(0);

                                    string SousFamilleNom = SousFamillesReader.GetString(1);

                                    TreeNode SousFamilleNode = new TreeNode(SousFamilleNom);

                                    // Récupérer les articles associés à cette sous-famille
                                    using (SQLiteCommand ArticlesCommand = new SQLiteCommand("SELECT Description FROM Articles WHERE RefSousFamille = @RefSousFamille", Connection))
                                    {
                                        ArticlesCommand.Parameters.AddWithValue("@RefSousFamille", RefSousFamille);

                                        using (SQLiteDataReader ArticlesReader = ArticlesCommand.ExecuteReader())
                                        {
                                            while (ArticlesReader.Read())
                                            {
                                                string DescriptionArticle = ArticlesReader.GetString(0);

                                                SousFamilleNode.Nodes.Add(DescriptionArticle); // Ajouter chaque article comme un nœud sous la sous-famille
                                            }
                                        }
                                    }

                                    FamilleNode.Nodes.Add(SousFamilleNode); // Ajouter chaque sous-famille (avec ses articles) comme un nœud sous la famille
                                }
                            }
                        }

                        FamillesAvecSousFamillesNode.Nodes.Add(FamilleNode); // Ajouter chaque famille (avec ses sous-familles et articles) comme un nœud sous "Familles avec Sous-Familles"
                    }
                }
            }

            TreeView1.Nodes.Add(FamillesAvecSousFamillesNode);


            // Ajouter le nœud "Marques" avec toutes les marques
            TreeNode MarquesNode = new TreeNode("Marques");

            using (SQLiteCommand Command = new SQLiteCommand("SELECT Nom FROM Marques", Connection))
            {
                using (SQLiteDataReader Reader = Command.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        MarquesNode.Nodes.Add(Reader.GetString(0)); // Ajouter chaque marque comme un nœud sous "Marques"
                    }
                }
            }

            TreeView1.Nodes.Add(MarquesNode);
        }

        /// <summary>
        /// Charge toutes les données des articles.
        /// </summary>
        private void LoadAllArticlesData()
        {
            string Query = @"SELECT RefArticle, Description, Familles.Nom AS Familles, SousFamilles.Nom AS SousFamilles, Marques.Nom AS Marques, Quantite
                        FROM Articles
                        LEFT JOIN SousFamilles ON Articles.RefSousFamille = SousFamilles.RefSousFamille
                        LEFT JOIN Familles ON SousFamilles.RefFamille = Familles.RefFamille
                        LEFT JOIN Marques ON Articles.RefMarque = Marques.RefMarque";

            ClearListViewColumnsAndItems();

            AddListViewColumns("RefArticle", "Description", "Familles", "Sous-familles", "Marques", "Quantite");

            ExecuteQueryAndPopulateListView(Query);
        }

        /// <summary>
        /// Charge les données des familles.
        /// </summary>
        private void LoadFamiliesData()
        {
            string Query = "SELECT Nom FROM Familles";

            ClearListViewColumnsAndItems();

            // Ajoutez la colonne "Nom"
            AddListViewColumns("Nom");

            ExecuteQueryAndPopulateListView(Query);
        }

        /// <summary>
        /// Charge les données des marques.
        /// </summary>
        private void LoadBrandsData()
        {
            string Query = "SELECT Nom FROM Marques";

            ClearListViewColumnsAndItems();

            // Ajoutez la colonne "Nom"
            AddListViewColumns("Nom");

            ExecuteQueryAndPopulateListView(Query);
        }

        /// <summary>
        /// Charge les données d'une marque spécifique.
        /// </summary>
        /// <param name="MarqueSelectionnee">La marque sélectionnée.</param>
        private void LoadBrandData(string MarqueSelectionnee)
        {
            string Query = "SELECT Nom FROM Marques WHERE Nom = @Marque";

            ClearListViewColumnsAndItems();

            // Ajoutez la colonne "Nom"
            AddListViewColumns("Nom");

            using (SQLiteCommand Command = new SQLiteCommand(Query, Connection))
            {
                Command.Parameters.AddWithValue("@Marque", MarqueSelectionnee);

                ExecuteQueryAndPopulateListViewWithParameters(Command);
            }
        }

        /// <summary>
        /// Charge les données d'une sous-famille spécifique.
        /// </summary>
        /// <param name="FamilleSelectionnee">La famille sélectionnée.</param>
        private void LoadSubFamilyData(string FamilleSelectionnee)
        {
            string Query = "SELECT Nom FROM SousFamilles WHERE RefFamille IN (SELECT RefFamille FROM Familles WHERE Nom = @Famille)";

            ClearListViewColumnsAndItems();

            // Ajoutez la colonne "Nom"
            AddListViewColumns("Nom");

            using (SQLiteCommand Command = new SQLiteCommand(Query, Connection))
            {
                Command.Parameters.AddWithValue("@Famille", FamilleSelectionnee);

                ExecuteQueryAndPopulateListViewWithParameters(Command);
            }
        }

        /// <summary>
        /// Charge les données des articles d'une sous-famille spécifique.
        /// </summary>
        /// <param name="SousFamilleSelectionnee">La sous-famille sélectionnée.</param>
        private void LoadArticlesDataBySubFamily(string SousFamilleSelectionnee)
        {
            string Query = @"SELECT A.RefArticle, A.Description, F.Nom AS Famille, SF.Nom AS SousFamille, M.Nom AS Marque, A.Quantite 
                     FROM Articles A
                     INNER JOIN SousFamilles SF ON A.RefSousFamille = SF.RefSousFamille 
                     INNER JOIN Familles F ON SF.RefFamille = F.RefFamille 
                     INNER JOIN Marques M ON A.RefMarque = M.RefMarque 
                     WHERE SF.Nom = @SousFamille";

            ClearListViewColumnsAndItems();

            // Ajoutez les colonnes appropriées
            AddListViewColumns("Référence", "Description", "Famille", "Sous-Famille", "Marque", "Quantité");

            using (SQLiteCommand Command = new SQLiteCommand(Query, Connection))
            {
                Command.Parameters.AddWithValue("@SousFamille", SousFamilleSelectionnee);

                ExecuteQueryAndPopulateListViewWithParameters(Command);
            }
        }

        /// <summary>
        /// Charge les données d'un article spécifique.
        /// </summary>
        /// <param name="SelectedArticleDescription">La description de l'article sélectionné.</param>
        private void LoadSelectedArticleData(string SelectedArticleDescription)
        {
            string Query = @"SELECT A.RefArticle, A.Description, F.Nom AS Familles, SF.Nom AS SousFamilles, M.Nom AS Marques, A.Quantite 
                     FROM Articles A 
                     INNER JOIN Familles F ON A.RefSousFamille = F.RefFamille 
                     INNER JOIN SousFamilles SF ON A.RefSousFamille = SF.RefSousFamille 
                     INNER JOIN Marques M ON A.RefMarque = M.RefMarque 
                     WHERE A.Description = @SelectedArticleDescription";

            ClearListViewColumnsAndItems();

            // Ajoutez les colonnes nécessaires
            AddListViewColumns("RefArticle", "Description", "Familles", "Sous-familles", "Marques", "Quantite");

            using (SQLiteCommand Command = new SQLiteCommand(Query, Connection))
            {
                Command.Parameters.AddWithValue("@SelectedArticleDescription", SelectedArticleDescription);

                ExecuteQueryAndPopulateListViewWithParameters(Command);
            }
        }

        /// <summary>
        /// Exécute une requête SQL et remplit le ListView avec les données retournées.
        /// </summary>
        /// <param name="Command">La requête SQL à exécuter.</param>
        private void ExecuteQueryAndPopulateListViewWithParameters(SQLiteCommand Command)
        {
            using (SQLiteDataReader Reader = Command.ExecuteReader())
            {
                while (Reader.Read())
                {
                    string[] SubItems = new string[Reader.FieldCount];

                    for (int Index = 0; Index < Reader.FieldCount; Index++)
                    {
                        object Value = Reader.GetValue(Index);

                        SubItems[Index] = Value != DBNull.Value ? Value.ToString() : string.Empty;
                    }

                    ListViewItem Item = new ListViewItem(SubItems);

                    ListView1.Items.Add(Item);

                    Console.WriteLine($"Total items in ListView: {ListView1.Items.Count}");
                }
            }
        }

        /// <summary>
        /// Efface les colonnes et les éléments du ListView.
        /// </summary>
        private void ClearListViewColumnsAndItems()
        {
            ListView1.Columns.Clear();

            ListView1.Items.Clear();
        }

        /// <summary>
        /// Ajoute des colonnes au ListView.
        /// </summary>
        /// <param name="ColumnNames">Les noms des colonnes à ajouter.</param>
        private void AddListViewColumns(params string[] ColumnNames)
        {

            foreach (var ColumnName in ColumnNames)
            {
                //ListView1.Columns.Add(ColumnName, 80);
                switch (ColumnName)
                {
                    case "RefArticle":

                        ListView1.Columns.Add(ColumnName, 80);

                        break;

                    case "Description":

                        ListView1.Columns.Add(ColumnName, 240);

                        break;

                    case "Familles":

                        ListView1.Columns.Add(ColumnName, 150);

                        break;

                    case "Sous-familles":

                        ListView1.Columns.Add(ColumnName, 190);

                        break;

                    case "Marques":

                        ListView1.Columns.Add(ColumnName, 80);

                        break;

                    case "Quantite":

                        ListView1.Columns.Add(ColumnName, 60);

                        break;
                    case "Nom":
                        
                        ListView1.Columns.Add(ColumnName, 190);
                        
                        break;
                    default:

                        ListView1.Columns.Add(ColumnName, 80);

                        break;
                }
            }
        }

        /// <summary>
        /// Exécute une requête SQL et remplit le ListView avec les données retournées.
        /// </summary>
        /// <param name="Query">La requête SQL à exécuter.</param>
        private void ExecuteQueryAndPopulateListView(string Query)
        {
            using (SQLiteCommand Command = new SQLiteCommand(Query, Connection))
            {
                using (SQLiteDataReader Reader = Command.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        try
                        {
                            // Initialiser un tableau pour stocker les valeurs des colonnes
                            string[] subItems = new string[reader.FieldCount];
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                // Tentative de récupération de la valeur
                                object value = reader.GetValue(i);
                                subItems[i] = value != DBNull.Value ? value.ToString() : "NULL";
                                Console.WriteLine($"Colonne {i}: {subItems[i]}");
                            }

                            // Créer et ajouter l'élément ListViewItem au ListView
                            ListViewItem item = new ListViewItem(subItems);
                            listView1.Items.Add(item);

                            // Création et affectation de l'objet Article au Tag de ListViewItem pour un usage ultérieur
                            Article article = new Article
                            {
                                RefArticle = reader.GetString(reader.GetOrdinal("RefArticle")),
                                Description = reader.GetString(reader.GetOrdinal("Description")),
                                // Assurez-vous que les noms des colonnes correspondent à ceux de votre base de données
                                Marque = new Marque { Nom = reader["Marques"].ToString() },
                                Famille = new Famille { Nom = reader["Familles"].ToString() },
                                SousFamille = new SousFamille { Nom = reader["SousFamilles"].ToString() },
                                PrixHT = reader.GetFloat(reader.GetOrdinal("PrixHT")),
                                Quantite = reader.GetInt32(reader.GetOrdinal("Quantite"))
                            };
                            item.Tag = article;
                        }
                        catch (Exception ex)
                        {
                            // Log de l'exception pour le débogage
                            Console.WriteLine($"Erreur lors de la création de l'élément ListViewItem: {ex.Message}");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gère l'événement de sélection d'un nœud dans l'arborescence.
        /// </summary>
        /// <param name="Sender">L'objet qui a déclenché l'événement.</param>
        /// <param name="Event">Les arguments de l'événement.</param>
        private void TreeView1_AfterSelect(object Sender, TreeViewEventArgs Event)
        {
            TreeNode SelectedNode = TreeView1.SelectedNode;

            if (SelectedNode != null)
            {
                string NodeName = SelectedNode.Text;

                switch (NodeName)
                {
                    case "Tous les articles":

                        LoadAllArticlesData();

                        break;

                    case "Marques":

                        LoadBrandsData();

                        break;

                    case "Familles":

                        LoadFamiliesData();

                        break;

                    default:
                        if (SelectedNode.Parent != null)
                        {
                            string ParentNodeName = SelectedNode.Parent.Text;

                            if (ParentNodeName == "Marques")
                            {
                                LoadBrandData(NodeName);
                            }
                            else if (ParentNodeName == "Familles")
                            {
                                LoadSubFamilyData(NodeName);
                            }
                            else if (SelectedNode.Parent.Parent != null && SelectedNode.Parent.Parent.Text == "Familles")
                            {
                                LoadArticlesDataBySubFamily(NodeName);
                            }
                            else if (ParentNodeName == "Tous les articles")
                            {
                                LoadSelectedArticleData(NodeName);
                            }
                            else
                            {
                                LoadSelectedArticleData(NodeName);
                            }
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Gère l'événement de clic sur le menu "Quitter".
        /// </summary>
        /// <param name="Sender">L'objet qui a déclenché l'événement.</param>
        /// <param name="Event">Les arguments de l'événement.</param>
        private void FormMain_FormClosing(object Sender, FormClosingEventArgs Event)
        {
            Properties.Settings.Default["WindowState"] = this.WindowState;

            Properties.Settings.Default["WindowLocation"] = this.Location;

            Properties.Settings.Default["WindowSize"] = this.Size;

            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Gère l'événement de chargement du formulaire principal.
        /// </summary>
        /// <param name="Sender">L'objet qui a déclenché l'événement.</param>
        /// <param name="Event">Les arguments de l'événement.</param>
        private void FormMain_Load(object Sender, EventArgs Event)
        {
            this.WindowState = Properties.Settings.Default.WindowState;

            // Restaurer la position et la taille de la fenêtre si elle n'était pas maximisée ou minimisée
            if (this.WindowState == FormWindowState.Normal)
            {
                this.Location = Properties.Settings.Default.WindowLocation;

                this.Size = Properties.Settings.Default.WindowSize;
            }

            // Vérifier que la fenêtre est visible sur l'écran
            if (!IsWindowVisibleOnScreen(this))
            {
                this.Location = new Point(100, 100);
            }

            UpdateStatusStrip();
        }

        /// <summary>
        /// Vérifie si la fenêtre est visible sur l'écran.
        /// </summary>
        /// <param name="Fenetre">La fenêtre à vérifier.</param>
        private bool IsWindowVisibleOnScreen(Form Fenetre)
        {
            Screen[] Screens = Screen.AllScreens;

            foreach (Screen Screen in Screens)
            {
                Rectangle FormRectangle = new Rectangle(Fenetre.Left, Fenetre.Top, Fenetre.Width, Fenetre.Height);

                if (Screen.WorkingArea.IntersectsWith(FormRectangle))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Met à jour la barre d'état du formulaire principal.
        /// </summary>
        private void UpdateStatusStrip()
        {
            ToolStripStatusLabelArticles.Text = $"Articles: {GetCountFromDatabase("SELECT COUNT(*) FROM Articles")}";

            ToolStripStatusLabelFamilles.Text = $"Familles: {GetCountFromDatabase("SELECT COUNT(*) FROM Familles")}";

            ToolStripStatusLabelSousFamilles.Text = $"Sous-Familles: {GetCountFromDatabase("SELECT COUNT(*) FROM SousFamilles")}";

            ToolStripStatusLabelMarques.Text = $"Marques: {GetCountFromDatabase("SELECT COUNT(*) FROM Marques")}";
        }

        /// <summary>
        /// Récupère le nombre d'éléments dans la base de données.
        /// </summary>
        /// <param name="Query">La requête SQL à exécuter.</param>
        private int GetCountFromDatabase(string Query)
        {
            using (var Command = new SQLiteCommand(Query, Connection))
            {
                return Convert.ToInt32(Command.ExecuteScalar());
            }
        }

        /// <summary>
        /// Met à jour la barre d'état du formulaire principal.
        /// </summary>
        public void UpdateMainFormStatusStrip()
        {
            UpdateStatusStrip();
        }

        /// <summary>
        /// Gère l'événement de clic sur le menu "Actualiser".
        /// </summary>
        /// <param name="Sender">L'objet qui a déclenché l'événement.</param>
        /// <param name="Event">Les arguments de l'événement.</param>
        private void ListView1_ColumnClick(object Sender, ColumnClickEventArgs Event)
        {
            if (Event.Column == IndexColonneDescription || Event.Column == IndexColonneFamille ||
                Event.Column == IndexColonneSousFamille || Event.Column == IndexColonneMarque)
            {
                GroupListViewItems(Event.Column);
            }
        }

        public void ActualiserDonnees()
        {
            PopulateTreeView();

            if (treeView1.Nodes.Count > 0)
            {
                treeView1.SelectedNode = treeView1.Nodes[0];
            }

            RefreshListView();

            Console.WriteLine("F5");
        }

        private void RefreshListView()
        {
            listView1.Items.Clear();
            listView1.Groups.Clear();

            string query = @"SELECT 
                                Articles.RefArticle, 
                                Articles.Description, 
                                Familles.Nom AS NomFamille, 
                                SousFamilles.Nom AS NomSousFamille, 
                                Marques.Nom AS NomMarque,  
                                Articles.Quantite,
                                Articles.PrixHT
                            FROM 
                                Articles
                                LEFT JOIN SousFamilles ON Articles.RefSousFamille = SousFamilles.RefSousFamille
                                LEFT JOIN Familles ON SousFamilles.RefFamille = Familles.RefFamille
                                LEFT JOIN Marques ON Articles.RefMarque = Marques.RefMarque";

            using (SQLiteCommand command = new SQLiteCommand(query, connection))
            {
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string[] subItems = {   reader["RefArticle"].ToString(),
                                        reader["Description"].ToString(),
                                        reader["NomFamille"].ToString(),
                                        reader["NomSousFamille"].ToString(),
                                        reader["NomMarque"].ToString(),
                                        reader["Quantite"].ToString(),
                                        string.Format("{0:0.00}", reader["PrixHT"])                                        
                                    };

                        ListViewItem item = new ListViewItem(subItems);

                        // Création de l'objet Article à associer
                        Article article = new Article
                        {
                            RefArticle = reader["RefArticle"].ToString(),
                            Description = reader["Description"].ToString(),
                            Famille = new Famille { Nom = reader["NomFamille"].ToString() },
                            SousFamille = new SousFamille { Nom = reader["NomSousFamille"].ToString() },
                            Marque = new Marque { Nom = reader["NomMarque"].ToString() },
                            Quantite = Convert.ToInt32(reader["Quantite"]),
                            PrixHT = Convert.ToSingle(reader["PrixHT"])
                        };

                        item.Tag = article; // Association de l'objet Article
                        listView1.Items.Add(item);
                    }
                }
            }
        }




        private void ActualiserToolStripMenuItem_Click(object Emetteur, EventArgs Evenement)
        {
            ActualiserDonnees();
        }

        /// <summary>
        /// Initialise le menu contextuel du ListView.
        /// </summary>
        private void InitializeContextMenuStrip()
        {
            // Créez le menu contextuel et ajoutez les options
            ContextMenuStrip = new ContextMenuStrip();

            ContextMenuStrip.Items.Add("Ajouter");

            ContextMenuStrip.Items.Add("Modifier");

            ContextMenuStrip.Items.Add("Supprimer");

            ContextMenuStrip.ItemClicked += ContextMenuStrip_ItemClicked;
        }

        /// <summary>
        /// Gère l'événement de double clic sur le menu "Ajouter un article".
        /// </summary>
        /// <param name="Sender">L'objet qui a déclenché l'événement.</param>
        /// <param name="Event">Les arguments de l'événement.</param>
        private void ListView1_MouseDoubleClick(object Sender, MouseEventArgs Event)
        {
            Article article = GetArticleFromSelectedItem();
            if (article != null)
            {
                Console.WriteLine($"Modification de l'article : {article.RefArticle}");
                FenetreArticle fenetre = new FenetreArticle(article);
                fenetre.ArticleUpdatedOrAdded += FenetreArticle_ArticleUpdatedOrAdded;
                fenetre.ShowDialog();
            }
            else
            {
                Console.WriteLine("Double clic sans sélection d'article.");
            }
        }

        private void listView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (Event.KeyCode == Keys.Enter || Event.KeyCode == Keys.Space)
            {
                Article article = GetArticleFromSelectedItem();
                if (article != null)
                {
                    FenetreArticle fenetre = new FenetreArticle(article);
                    fenetre.ShowDialog();
                }
                else
                {
                    FenetreArticle fenetre = new FenetreArticle();
                    fenetre.Text = "Créer un nouvel article";
                    fenetre.ShowDialog();
                }
            }
        }

        private Article GetArticleFromSelectedItem()
        {
            if (listView1.SelectedItems.Count > 0)
            {
                var article = listView1.SelectedItems[0].Tag as Article;
                Console.WriteLine($"Article sélectionné : {article?.RefArticle}");
                return article;
            }
            Console.WriteLine("Aucun article sélectionné.");
            return null;
        }

        private void listView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (Event.Button == MouseButtons.Right)
            {
                Point MousePosition = ListView1.PointToClient(Control.MousePosition);

                // Réinitialisez la visibilité des options du menu
                for (int Index = 0; Index < ContextMenuStrip.Items.Count; Index++)
                {
                    ContextMenuStrip.Items[Index].Enabled = true;
                }

                // Vérifiez si une ligne a été cliquée
                ListViewHitTestInfo HitTestInfo = ListView1.HitTest(MousePosition);

                if (HitTestInfo.Item != null)
                {
                    // Si une ligne a été cliquée, affichez le menu contextuel complet
                    ListViewItem SelectedItem = HitTestInfo.Item;

                    SetContextMenuOptions(SelectedItem);

                    ContextMenuStrip.Show(ListView1, MousePosition);
                }
                else
                {
                    // Si le clic est en dehors d'une ligne, affichez seulement la première option du menu
                    for (int Index = 1; Index < ContextMenuStrip.Items.Count; Index++)
                    {
                        ContextMenuStrip.Items[Index].Enabled = false;
                    }
                    ContextMenuStrip.Show(ListView1, MousePosition);
                }
            }
        }

        /// <summary>
        /// Définit les options du menu contextuel en fonction de l'élément sélectionné.
        /// </summary>
        /// <param name="SelectedItem">L'élément sélectionné dans le ListView.</param>
        private void SetContextMenuOptions(ListViewItem SelectedItem)
        {
            TreeNode ParentNode = SelectedItem.Tag as TreeNode;

            if (ParentNode != null)
            {
                string ParentItemText = ParentNode.Text;

                // Vérifiez le type d'objet sélectionné en fonction du nœud parent
                switch (ParentItemText)
                {
                    case "Marques":

                        ContextMenuStrip.Items[0].Text = $"Option 1 pour {SelectedItem.Text}";

                        ContextMenuStrip.Items[1].Text = $"Option 2 pour {SelectedItem.Text}";

                        ContextMenuStrip.Items[2].Text = $"Option 3 pour {SelectedItem.Text}";

                        break;

                    case "Familles":
                    case "Tous les articles":

                        ContextMenuStrip.Items[0].Text = $"Option 1 pour {SelectedItem.Text}";

                        ContextMenuStrip.Items[1].Text = $"Option 2 pour {SelectedItem.Text}";

                        ContextMenuStrip.Items[2].Text = $"Option 3 pour {SelectedItem.Text}";

                        break;

                    case "Sous-Familles":

                        ContextMenuStrip.Items[0].Text = $"Option 1 pour {SelectedItem.Text}";

                        ContextMenuStrip.Items[1].Text = $"Option 2 pour {SelectedItem.Text}";

                        ContextMenuStrip.Items[2].Text = $"Option 3 pour {SelectedItem.Text}";

                        break;

                    default:

                        ContextMenuStrip.Items[0].Text = "Option 1 par défaut";

                        ContextMenuStrip.Items[1].Text = "Option 2 par défaut";

                        ContextMenuStrip.Items[2].Text = "Option 3 par défaut";

                        break;
                }
            }
        }

        private void ContextMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Text == "Ajouter")
            {
                FenetreArticle fenetre = new FenetreArticle();
                fenetre.Show();
            }
            else if (e.ClickedItem.Text == "Modifier")
            {
                Article article = GetArticleFromSelectedItem();
                if (article != null)
                {
                    FenetreArticle fenetre = new FenetreArticle(article);
                    fenetre.Show();
                }
            }
            else if (Event.ClickedItem.Text == "Supprimer")
            {
                Article article = GetArticleFromSelectedItem();
                if (article != null && MessageBox.Show("Êtes-vous sûr de vouloir supprimer l'article ?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    // Code pour supprimer l'article de la base de données
                    DeleteArticle(article);
                }
            }
            contextMenuStrip.Close();
        }

        private void DeleteArticle(Article article)
        {
            if (article != null && !string.IsNullOrEmpty(article.RefArticle))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                try
                {
                    using (var command = new SQLiteCommand("DELETE FROM Articles WHERE RefArticle = @RefArticle", connection))
                    {
                        command.Parameters.AddWithValue("@RefArticle", article.RefArticle);
                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            MessageBox.Show("L'article a été supprimé avec succès.", "Suppression réussie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Aucun article correspondant trouvé pour la suppression.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Une erreur est survenue lors de la suppression de l'article : {ex.Message}", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }

                    ActualiserDonnees();
                }
            }
        }


        private void Ajouter_Click(object sender, EventArgs e)
        {
            
        }

        private void Modifier_Click(object sender, EventArgs e)
        {
            
        }

        private void Supprimer_Click(object sender, EventArgs e)
        {
            
        }

        private void GroupListViewItems(int colIndex)
        {
            List<ListViewGroup> Groups = new List<ListViewGroup>();

            var Items = ListView1.Items.Cast<ListViewItem>().ToList();

            // Effacer les éléments et les groupes du ListView pour réinitialiser
            ListView1.Items.Clear();

            ListView1.Groups.Clear();

            foreach (var Item in Items)
            {
                string GroupKey = GetGroupKey(Item, ColIndex);

                ListViewGroup Group = Groups.Find(g => g.Header == GroupKey);

                if (Group == null)
                {
                    Group = new ListViewGroup(GroupKey, GroupKey);

                    Groups.Add(Group);
                }

                Item.Group = Group;

                ListView1.Items.Add(Item); 
            }

            // Trier les groupes
            Groups.Sort((g1, g2) => String.Compare(g1.Header, g2.Header, StringComparison.Ordinal));

            // Ajouter les groupes triés au ListView
            foreach (var Group in Groups)
            {
                ListView1.Groups.Add(Group);
            }

            ListView1.Sort();
        }

        /// <summary>
        /// Obtient la clé de groupe pour un élément donné.
        /// </summary>
        /// <param name="Item">L'élément pour lequel obtenir la clé de groupe.</param>
        /// <param name="ColIndex">L'index de la colonne à utiliser pour le regroupement.</param>
        private string GetGroupKey(ListViewItem Item, int ColIndex)
        {
            if (ColIndex == IndexColonneDescription)
            {
                return Item.SubItems[ColIndex].Text[0].ToString().ToUpper();
            }
            else
            {
                return Item.SubItems[ColIndex].Text;
            }
        }

        private void FenetreArticle_ArticleUpdatedOrAdded(object sender, EventArgs e)
        {
            ActualiserDonnees();
        }

    }
}
