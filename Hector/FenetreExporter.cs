﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;

/// <summary>
/// Fenêtre d'exportation des articles de la base de données vers un fichier CSV.
/// </summary>
/// <remarks>
/// Cette fenêtre permet d'exporter les articles de la base de données vers un fichier CSV.
/// </remarks>
namespace Hector
{
    /// <summary>
    /// Fenêtre d'exportation des articles de la base de données vers un fichier CSV.
    /// </summary>
    public partial class FenetreExporter : Form
    {
        private DatabaseManager DbManager;

        /// <summary>
        /// Constructeur de la fenêtre d'exportation.
        /// </summary>
        public FenetreExporter()
        {
            InitializeComponent();

            string SolutionDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;

            string DatabaseFileName = "HectorTest.SQLite";

            string AbsolutePath = Path.Combine(SolutionDirectory, "ExternalFiles", DatabaseFileName);

            DbManager = new DatabaseManager(AbsolutePath);
        }

        /// <summary>
        /// Gestionnaire de l'événement de clic sur le bouton d'exportation.
        /// </summary>
        /// <param name="Sender">Objet à l'origine de l'événement.</param>
        /// <param name="Event">Arguments de l'événement.</param>
        private void ExporterButton_Click(object Sender, EventArgs Event)
        {
            if (!string.IsNullOrEmpty(TextBoxFilePathExporter.Text))
            {
                var Articles = DbManager.GetAllArticles();

                StringBuilder CsvBuilder = new StringBuilder();

                CsvBuilder.AppendLine("Description;Ref;Marque;Famille;Sous-Famille;Prix H.T.");

                foreach (var Article in Articles)
                {
                    string PrixFormatted = Article.PrixHT.ToString("0.00", CultureInfo.InvariantCulture).Replace('.', ',');

                    CsvBuilder.AppendLine($"{Article.Description};{Article.RefArticle};{Article.Marque.Nom};{Article.Famille.Nom};{Article.SousFamille.Nom};{PrixFormatted}");
                }

                File.WriteAllText(TextBoxFilePathExporter.Text, CsvBuilder.ToString(), Encoding.UTF8);

                MessageBox.Show("L'exportation a été réalisée avec succès.", "Exportation réussie", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner un fichier pour l'exportation.", "Fichier non sélectionné", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Gestionnaire de l'événement de clic sur le bouton d'annulation.
        /// </summary>
        /// <param name="Sender">Objet à l'origine de l'événement.</param>
        /// <param name="Event">Arguments de l'événement.</param>
        private void AnnulerButton_Click(object Sender, EventArgs Event)
        {
            this.Close();
        }

        /// <summary>
        /// Gestionnaire de l'événement de clic sur le bouton de sélection du fichier.
        /// </summary>
        /// <param name="Sender">Objet à l'origine de l'événement.</param>
        /// <param name="Event">Arguments de l'événement.</param>
        private void SelectFichierButton_Click(object Sender, EventArgs Event)
        {
            SaveFileDialog SaveFileDialog = new SaveFileDialog
            {
                Filter = "CSV Files (*.csv)|*.csv",

                Title = "Sélectionner un emplacement pour le fichier CSV"
            };

            if (SaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                TextBoxFilePathExporter.Text = SaveFileDialog.FileName;
            }
        }
    }
}
