﻿
using System.Windows.Forms;
using Hector.model;


/// <summary>
/// Représente la fenêtre d'un article.
/// </summary>
namespace Hector
{
    /// <summary>
    /// Fenêtre d'un article.
    /// </summary>
    public partial class FenetreArticle : Form
    {
        private SQLiteConnection connection;
        private Article articleActuel;

        public event EventHandler ArticleUpdatedOrAdded;

        public FenetreArticle()
        {
            InitializeComponent();
        }

        public FenetreArticle(Article article)
        {
            InitializeComponent();
            articleActuel = article;

            Console.WriteLine($"Ref: {articleActuel.RefArticle}, Famille: {articleActuel.Famille?.Nom}, SousFamille: {articleActuel.SousFamille?.Nom}, Marque: {articleActuel.Marque?.Nom}");
            ChargerDonneesArticle();
        }


        private void FenetreArticle_Load(object sender, EventArgs e)
        {
            InitializeDatabaseConnection();
            ChargerDonneesArticle();
            ChargerComboBoxes();
        }

        private void InitializeDatabaseConnection()
        {
            string solutionDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string databaseFileName = "HectorTest.SQLite";
            string absolutePath = Path.Combine(solutionDirectory, "ExternalFiles", databaseFileName);
            string connectionString = $"Data Source={absolutePath};Version=3;Charset=utf8;";
            connection = new SQLiteConnection(connectionString);
            connection.Open();
        }

        private void ChargerDonneesArticle()
        {
            if (articleActuel != null)
            {
                textBoxRef.Text = articleActuel.RefArticle;
                textBoxDesc.Text = articleActuel.Description;

                numericUpDownPrix.DecimalPlaces = 2;
                numericUpDownPrix.Increment = 0.01M;

                numericUpDownPrix.Value = Convert.ToDecimal(articleActuel.PrixHT);
                numericUpDownQt.Value = articleActuel.Quantite;

            }
        }

        private void ChargerComboBoxes()
        {
            // Charger les familles
            ChargerComboBox("SELECT Nom FROM Familles", comboBoxFam, articleActuel?.Famille.Nom);
            Console.WriteLine($"Famille sélectionnée: {comboBoxFam.SelectedItem}");

            // Charger les sous-familles
            ChargerComboBox("SELECT Nom FROM SousFamilles", comboBoxSsFam, articleActuel?.SousFamille.Nom);
            Console.WriteLine($"SousFamille sélectionnée: {comboBoxSsFam.SelectedItem}");

            // Charger les marques
            ChargerComboBox("SELECT Nom FROM Marques", comboBoxMarq, articleActuel?.Marque.Nom);
            Console.WriteLine($"Marque sélectionnée: {comboBoxMarq.SelectedItem}");
        }

        private void ChargerComboBox(string query, ComboBox comboBox, string valeurActuelle)
        {
            using (var command = new SQLiteCommand(query, connection))
            {
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string nom = reader.GetString(0);
                        comboBox.Items.Add(nom);
                        if (nom == valeurActuelle)
                        {
                            comboBox.SelectedItem = nom;
                        }
                    }
                }
            }
        }

        private void buttonValider_Click(object sender, EventArgs e)
        {
            if (!ValiderDonnees())
            {
                MessageBox.Show("Certains champs ne sont pas correctement remplis.");
                return;
            }

            if (articleActuel == null) // Si c'est un nouvel article
            {
                // Création d'un nouvel objet Article et le remplir avec les données
                Article nouvelArticle = new Article
                {
                    RefArticle = textBoxRef.Text,
                    Description = textBoxDesc.Text,
                    Marque = new Marque { Nom = comboBoxMarq.SelectedItem.ToString() },
                    Famille = new Famille { Nom = comboBoxFam.SelectedItem.ToString() },
                    SousFamille = new SousFamille { Nom = comboBoxSsFam.SelectedItem.ToString() },
                    PrixHT = float.Parse(numericUpDownPrix.Value.ToString()),
                    Quantite = Convert.ToInt32(numericUpDownQt.Value)
                };

                // Insérer le nouvel article dans la base de données
                InsertArticle(nouvelArticle);
            }
            else // Si c'est une modification
            {
                // Mettre à jour l'objet articleActuel avec les nouvelles données
                articleActuel.RefArticle = textBoxRef.Text;
                articleActuel.Description = textBoxDesc.Text;
                articleActuel.Marque.Nom = comboBoxMarq.SelectedItem.ToString();
                articleActuel.Famille.Nom = comboBoxFam.SelectedItem.ToString();
                articleActuel.SousFamille.Nom = comboBoxSsFam.SelectedItem.ToString();
                articleActuel.PrixHT = float.Parse(numericUpDownPrix.Value.ToString());
                articleActuel.Quantite = Convert.ToInt32(numericUpDownQt.Value);

                // Mettre à jour l'article dans la base de données
                UpdateArticle(articleActuel);
            }

            // Fermer la fenêtre après enregistrement
            this.Close();
        }

        private bool ValiderDonnees()
        {
            bool refValide = !string.IsNullOrWhiteSpace(textBoxRef.Text) && textBoxRef.Text.Length <= 8;
            Console.WriteLine($"Référence valide : {refValide}");

            bool descValide = textBoxDesc.Text != "" && textBoxDesc.Text.Length <= 100;
            Console.WriteLine($"Description valide : {descValide}");

            bool famValide = comboBoxFam.SelectedItem != null;
            Console.WriteLine($"Famille sélectionnée : {famValide}");

            bool ssFamValide = comboBoxSsFam.SelectedItem != null;
            Console.WriteLine($"Sous-Famille sélectionnée : {ssFamValide}");

            bool marqueValide = comboBoxMarq.SelectedItem != null;
            Console.WriteLine($"Marque sélectionnée : {marqueValide}");

            bool prixValide = numericUpDownPrix.Value > 0;
            Console.WriteLine($"Prix valide : {prixValide}");

            bool qtValide = numericUpDownQt.Value > 0;
            Console.WriteLine($"Quantité valide : {qtValide}");

            return refValide && descValide && famValide && ssFamValide && marqueValide && prixValide && qtValide;
        }

        private void InsertArticle(Article article)
        {
            string insertQuery = @"INSERT INTO Articles (RefArticle, Description, RefMarque, RefSousFamille, PrixHT, Quantite) 
                                   VALUES (@RefArticle, @Description, 
                                   (SELECT RefMarque FROM Marques WHERE Nom = @Marque), 
                                   (SELECT RefSousFamille FROM SousFamilles WHERE Nom = @SousFamille AND RefFamille = (SELECT RefFamille FROM Familles WHERE Nom = @Famille)), 
                                   @PrixHT, @Quantite)";

            using (var command = new SQLiteCommand(insertQuery, connection))
            {
                // Définir les paramètres de la commande pour éviter l'injection SQL
                command.Parameters.AddWithValue("@RefArticle", article.RefArticle);
                command.Parameters.AddWithValue("@Description", article.Description);
                command.Parameters.AddWithValue("@Marque", article.Marque.Nom);
                command.Parameters.AddWithValue("@Famille", article.Famille.Nom);
                command.Parameters.AddWithValue("@SousFamille", article.SousFamille.Nom);
                command.Parameters.AddWithValue("@PrixHT", article.PrixHT);
                command.Parameters.AddWithValue("@Quantite", article.Quantite);

                // Exécutez la commande
                command.ExecuteNonQuery();
            }
        }


        private void UpdateArticle(Article article)
        {
            string updateQuery = @"UPDATE Articles 
                                    SET Description = @Description, 
                                        RefMarque = (SELECT RefMarque FROM Marques WHERE Nom = @Marque), 
                                        RefSousFamille = (SELECT RefSousFamille FROM SousFamilles WHERE Nom = @SousFamille AND RefFamille = (SELECT RefFamille FROM Familles WHERE Nom = @Famille)), 
                                        PrixHT = @PrixHT, 
                                        Quantite = @Quantite 
                                    WHERE RefArticle = @RefArticle";

            using (var command = new SQLiteCommand(updateQuery, connection))
            {
                // Définir les paramètres de la commande pour éviter l'injection SQL
                command.Parameters.AddWithValue("@RefArticle", article.RefArticle);
                command.Parameters.AddWithValue("@Description", article.Description);
                command.Parameters.AddWithValue("@Marque", article.Marque.Nom);
                command.Parameters.AddWithValue("@Famille", article.Famille.Nom);
                command.Parameters.AddWithValue("@SousFamille", article.SousFamille.Nom);
                command.Parameters.AddWithValue("@PrixHT", article.PrixHT);
                command.Parameters.AddWithValue("@Quantite", article.Quantite);

                // Exécutez la commande
                command.ExecuteNonQuery();

                OnArticleUpdatedOrAdded();
            }
        }

        protected virtual void OnArticleUpdatedOrAdded()
        {
            ArticleUpdatedOrAdded?.Invoke(this, EventArgs.Empty);
        }
    }
}
