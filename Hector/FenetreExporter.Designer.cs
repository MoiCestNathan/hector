﻿
namespace Hector
{
    partial class FenetreExporter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool Disposing)
        {
            if (Disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(Disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SelectFichierButton = new System.Windows.Forms.Button();
            this.TextBoxFilePathExporter = new System.Windows.Forms.TextBox();
            this.AnnulerButton = new System.Windows.Forms.Button();
            this.ExporterButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SelectFichierButton
            // 
            this.SelectFichierButton.Location = new System.Drawing.Point(45, 38);
            this.SelectFichierButton.Name = "SelectFichierButton";
            this.SelectFichierButton.Size = new System.Drawing.Size(110, 47);
            this.SelectFichierButton.TabIndex = 0;
            this.SelectFichierButton.Text = "Sélectionner fichier .csv";
            this.SelectFichierButton.UseVisualStyleBackColor = true;
            this.SelectFichierButton.Click += new System.EventHandler(this.SelectFichierButton_Click);
            // 
            // TextBoxFilePathExporter
            // 
            this.TextBoxFilePathExporter.Location = new System.Drawing.Point(173, 50);
            this.TextBoxFilePathExporter.Name = "TextBoxFilePathExporter";
            this.TextBoxFilePathExporter.Size = new System.Drawing.Size(571, 22);
            this.TextBoxFilePathExporter.TabIndex = 2;
            // 
            // AnnulerButton
            // 
            this.AnnulerButton.Location = new System.Drawing.Point(173, 289);
            this.AnnulerButton.Name = "AnnulerButton";
            this.AnnulerButton.Size = new System.Drawing.Size(75, 23);
            this.AnnulerButton.TabIndex = 3;
            this.AnnulerButton.Text = "Annuler";
            this.AnnulerButton.UseVisualStyleBackColor = true;
            this.AnnulerButton.Click += new System.EventHandler(this.AnnulerButton_Click);
            // 
            // ExporterButton
            // 
            this.ExporterButton.Location = new System.Drawing.Point(527, 289);
            this.ExporterButton.Name = "ExporterButton";
            this.ExporterButton.Size = new System.Drawing.Size(75, 23);
            this.ExporterButton.TabIndex = 4;
            this.ExporterButton.Text = "Exporter";
            this.ExporterButton.UseVisualStyleBackColor = true;
            this.ExporterButton.Click += new System.EventHandler(this.ExporterButton_Click);
            // 
            // FenetreExporter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ExporterButton);
            this.Controls.Add(this.AnnulerButton);
            this.Controls.Add(this.TextBoxFilePathExporter);
            this.Controls.Add(this.SelectFichierButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FenetreExporter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Exporter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SelectFichierButton;
        private System.Windows.Forms.TextBox TextBoxFilePathExporter;
        private System.Windows.Forms.Button AnnulerButton;
        private System.Windows.Forms.Button ExporterButton;
    }
}