﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Hector.model;

namespace Hector.DAO
{

    
    public class ArticleDAOImpl : IArticleDAO
    {
        private string connectionString = "";

        public void Delete(string refArticle)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("DELETE FROM Articles WHERE RefArticle = @RefArticle", connection);
                command.Parameters.AddWithValue("@RefArticle", refArticle);
                command.ExecuteNonQuery();
            }
        }

        public List<Article> GetAll()
        {
            List<Article> articles = new List<Article>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Articles", connection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Article article = new Article();
                    article.RefArticle = reader["RefArticle"].ToString();
                    article.Description = reader["Description"].ToString();
                    article.Famille = new Famille { Nom = reader["Famille"].ToString() };
                    article.SousFamille = new SousFamille { Nom = reader["SousFamille"].ToString() };
                    article.Marque = new Marque { Nom = reader["Marque"].ToString() };
                    article.Quantite = Convert.ToInt32(reader["Quantite"]);
                    article.PrixHT = Convert.ToInt32(reader["PrixHT"]);
                    articles.Add(article);
                }
            }
            return articles;
        }

        public Article GetByRefArticle(string refArticle)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Articles WHERE RefArticle = @RefArticle", connection);
                command.Parameters.AddWithValue("@RefArticlee", refArticle);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    Article article = new Article();
                    article.RefArticle = reader["RefArticle"].ToString();
                    article.Description = reader["Description"].ToString();
                    article.Famille = new Famille { Nom = reader["Famille"].ToString() };
                    article.SousFamille = new SousFamille { Nom = reader["SousFamille"].ToString() };
                    article.Marque = new Marque { Nom = reader["Marque"].ToString() };
                    article.Quantite = Convert.ToInt32(reader["Quantite"]);
                    article.PrixHT = Convert.ToInt32(reader["PrixHT"]);
                    return article;
                }
                return null;
            }
        }

        public void Insert(Article article)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("INSERT INTO Articles (RefArticle, Description, Famille, SousFamille, Marque, Quantite, PrixHT) VALUES (@Reference, @Description, @Famille, @SousFamille, @Marque, @Quantite, @PrixHT)", connection);
                command.Parameters.AddWithValue("@Reference", article.RefArticle);
                command.Parameters.AddWithValue("@Description", article.Description);
                command.Parameters.AddWithValue("@Famille", article.Famille.Nom);
                command.Parameters.AddWithValue("@SousFamille", article.SousFamille.Nom);
                command.Parameters.AddWithValue("@Marque", article.Marque.Nom);
                command.Parameters.AddWithValue("@Quantite", article.Quantite);
                command.Parameters.AddWithValue("@PrixTH", article.PrixHT);
                command.ExecuteNonQuery();
            }
        }

        public void Update(Article article)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("UPDATE Articles SET Description = @Description, Famille = @Famille, SousFamille = @SousFamille, Marque = @Marque, Quantite = @Quantite, PrixHT = @PrixHT WHERE RefArticle = @RefArticle", connection);
                command.Parameters.AddWithValue("@Description", article.Description);
                command.Parameters.AddWithValue("@Famille", article.Famille.Nom);
                command.Parameters.AddWithValue("@SousFamille", article.SousFamille.Nom);
                command.Parameters.AddWithValue("@Marque", article.Marque.Nom);
                command.Parameters.AddWithValue("@Quantite", article.Quantite);
                command.Parameters.AddWithValue("@PrixHT", article.PrixHT);
                command.Parameters.AddWithValue("@RefArticle", article.RefArticle);
                command.ExecuteNonQuery();
            }
        }
    }
}

