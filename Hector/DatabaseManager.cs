﻿using Hector.model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;

/// <summary>
/// Classe permettant de gérer la base de données SQLite
/// </summary>
/// <remarks>
/// Elle permet de créer les tables nécessaires, d'insérer des données et de les récupérer
/// </remarks>
namespace Hector
{
    /// <summary>
    /// Cette classe permet de gérer la base de données SQLite
    /// </summary>
    public class DatabaseManager
    {
        private readonly string ConnectionString;

        /// <summary>
        /// Constructeur de la classe DatabaseManager
        /// </summary>
        /// <param name="DbPath">Chemin de la base de données</param>
        public DatabaseManager(string DbPath)
        {
            this.ConnectionString = $"Data Source={DbPath};Version=3;";
        }

        /// <summary>
        /// S'assure que la table Marques existe
        /// </summary>
        /// <param name="NomMarque">Nom de la marque</param>
        /// <returns>Référence de la marque</returns>
        public int EnsureMarqueExists(string NomMarque)
        {
            try
            {
                using (var Connection = new SQLiteConnection(ConnectionString))
                {
                    Connection.Open();

                    var Command = new SQLiteCommand("SELECT RefMarque FROM Marques WHERE Nom = @Nom", Connection);

                    Command.Parameters.AddWithValue("@Nom", NomMarque);

                    var Result = Command.ExecuteScalar();

                    if (Result != null)
                    {
                        return Convert.ToInt32(Result);
                    }

                    Command = new SQLiteCommand("INSERT INTO Marques (Nom) VALUES (@Nom); SELECT last_insert_rowid();", Connection);

                    Command.Parameters.AddWithValue("@Nom", NomMarque);

                    return Convert.ToInt32(Command.ExecuteScalar());
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine($"Error in EnsureMarqueExists: {Ex.Message}");

                throw;
            }
        }

        /// <summary>
        /// S'assure que la table Familles existe
        /// </summary>
        /// <param name="NomFamille">Nom de la famille</param>
        /// <returns>Référence de la famille</returns>
        public int EnsureFamilleExists(string NomFamille)
        {
            using (var Connection = new SQLiteConnection(ConnectionString))
            {
                Connection.Open();

                var Command = new SQLiteCommand("SELECT RefFamille FROM Familles WHERE Nom = @Nom", Connection);

                Command.Parameters.AddWithValue("@Nom", NomFamille);

                var Result = Command.ExecuteScalar();

                if (Result != null)
                {
                    return Convert.ToInt32(Result);
                }

                Command = new SQLiteCommand("INSERT INTO Familles (Nom) VALUES (@Nom); SELECT last_insert_rowid();", Connection);

                Command.Parameters.AddWithValue("@Nom", NomFamille);

                return Convert.ToInt32(Command.ExecuteScalar());
            }
        }

        /// <summary>
        /// S'assure que la table SousFamilles existe
        /// </summary>
        /// <param name="NomSousFamille">Nom de la sous-famille</param>
        /// <param name="RefFamille">Référence de la famille</param>
        /// <returns>Référence de la sous-famille</returns>
        public int EnsureSousFamilleExists(string NomSousFamille, int RefFamille)
        {
            using (var Connection = new SQLiteConnection(ConnectionString))
            {
                Connection.Open();

                var Command = new SQLiteCommand("SELECT RefSousFamille FROM SousFamilles WHERE Nom = @Nom AND RefFamille = @RefFamille", Connection);

                Command.Parameters.AddWithValue("@Nom", NomSousFamille);

                Command.Parameters.AddWithValue("@RefFamille", RefFamille);

                var Result = Command.ExecuteScalar();

                if (Result != null)
                {
                    return Convert.ToInt32(Result);
                }

                Command = new SQLiteCommand("INSERT INTO SousFamilles (Nom, RefFamille) VALUES (@Nom, @RefFamille); SELECT last_insert_rowid();", Connection);

                Command.Parameters.AddWithValue("@Nom", NomSousFamille);

                Command.Parameters.AddWithValue("@RefFamille", RefFamille);

                return Convert.ToInt32(Command.ExecuteScalar());
            }
        }

        /// <summary>
        /// Méthode permettant de créer les tables nécessaires
        /// </summary>
        /// <param name="RefArticle">Référence de l'article</param>
        /// <param name="Description">Description de l'article</param>
        /// <param name="RefSousFamille">Référence de la sous-famille</param>
        /// <param name="RefMarque">Référence de la marque</param>
        /// <param name="PrixHT">Prix hors taxe</param>
        /// <param name="Quantite">Quantité</param>
        public void InsertArticle(string RefArticle, string Description, int RefSousFamille, int RefMarque, float PrixHT, int Quantite)
        {
            Console.WriteLine($"Inserting Article: {RefArticle}");

            using (var Connection = new SQLiteConnection(ConnectionString))
            {
                Connection.Open();

                var Command = new SQLiteCommand("INSERT INTO Articles (RefArticle, Description, RefSousFamille, RefMarque, PrixHT, Quantite) VALUES (@RefArticle, @Description, @RefSousFamille, @RefMarque, @PrixHT, @Quantite)", Connection);

                Command.Parameters.AddWithValue("@RefArticle", RefArticle);

                Command.Parameters.AddWithValue("@Description", Description);

                Command.Parameters.AddWithValue("@RefSousFamille", RefSousFamille);

                Command.Parameters.AddWithValue("@RefMarque", RefMarque);

                Command.Parameters.AddWithValue("@PrixHT", PrixHT);

                Command.Parameters.AddWithValue("@Quantite", Quantite);

                Command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Méthode permettant de vider la base de données
        /// </summary>
        public void ViderLaBaseDeDonnees()
        {
            List<string> Tables = new List<string> { "Articles", "Familles", "Marques", "SousFamilles", "sqlite_sequence" };

            using (var Connection = new SQLiteConnection(ConnectionString))
            {
                Connection.Open();

                using (var Transaction = Connection.BeginTransaction())
                {
                    foreach (var Table in Tables)
                    {
                        var Command = Connection.CreateCommand();

                        Command.CommandText = $"DELETE FROM {Table};";

                        Command.ExecuteNonQuery();
                    }
                    Transaction.Commit();
                }
            }
        }

        /// <summary>
        /// Méthode permettant de récupérer tous les articles
        /// </summary>
        /// <returns>Liste d'articles</returns>
        public List<Article> GetAllArticles()
        {
            List<Article> Articles = new List<Article>();

            string QueryString = @"
                                SELECT 
                                    Articles.RefArticle, 
                                    Articles.Description, 
                                    Marques.Nom AS Marque, 
                                    Familles.Nom AS Famille, 
                                    SousFamilles.Nom AS SousFamille, 
                                    Articles.PrixHT
                                FROM 
                                    Articles
                                    INNER JOIN Marques ON Articles.RefMarque = Marques.RefMarque
                                    INNER JOIN SousFamilles ON Articles.RefSousFamille = SousFamilles.RefSousFamille
                                    INNER JOIN Familles ON SousFamilles.RefFamille = Familles.RefFamille";

            using (var Connection = new SQLiteConnection(ConnectionString))
            {
                Connection.Open();

                using (var Command = new SQLiteCommand(QueryString, Connection))
                {
                    using (var Reader = Command.ExecuteReader())
                    {
                        while (Reader.Read())
                        {
                            var Article = new Article();
                            {
                                /*RefArticle = reader["RefArticle"].ToString(),
                                Description = reader["Description"].ToString(),
                                Marque = (model.Marque)reader["Marque"],
                                Famille = (model.Famille)reader["Famille"],
                                SousFamille = (model.SousFamille)reader["SousFamille"],
                                PrixHT = Convert.ToSingle(reader["PrixHT"])*/
                                Article.RefArticle = Reader["RefArticle"].ToString();

                                Article.Description = Reader["Description"].ToString();

                                Article.Famille = new Famille { Nom = Reader["Famille"].ToString() };

                                Article.SousFamille = new SousFamille { Nom = Reader["SousFamille"].ToString() };

                                Article.Marque = new Marque { Nom = Reader["Marque"].ToString() };
                                //article.Quantite = Convert.ToInt32(reader["Quantite"]);

                                Article.PrixHT = Convert.ToSingle(Reader["PrixHT"]);

                            };
                            Articles.Add(Article);
                        }
                    }
                }
            }

            return Articles;
        }
    }
}
