﻿
namespace Hector
{
    partial class FenetreArticle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool Disposing)
        {
            if (Disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(Disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.LabelRef = new System.Windows.Forms.Label();
            this.LabelDesc = new System.Windows.Forms.Label();
            this.LabelFam = new System.Windows.Forms.Label();
            this.LabelSsFam = new System.Windows.Forms.Label();
            this.LabelMarq = new System.Windows.Forms.Label();
            this.LabelPrix = new System.Windows.Forms.Label();
            this.LabelQt = new System.Windows.Forms.Label();
            this.ComboBoxFam = new System.Windows.Forms.ComboBox();
            this.ComboBoxSsFam = new System.Windows.Forms.ComboBox();
            this.ComboBoxMarq = new System.Windows.Forms.ComboBox();
            this.TextBoxRef = new System.Windows.Forms.TextBox();
            this.TextBoxDesc = new System.Windows.Forms.TextBox();
            this.NumericUpDownPrix = new System.Windows.Forms.NumericUpDown();
            this.NumericUpDownQt = new System.Windows.Forms.NumericUpDown();
            this.ButtonValider = new System.Windows.Forms.Button();
            this.TableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPrix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownQt)).BeginInit();
            this.SuspendLayout();
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.ColumnCount = 2;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.39014F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.60986F));
            this.TableLayoutPanel1.Controls.Add(this.LabelRef, 0, 0);
            this.TableLayoutPanel1.Controls.Add(this.LabelDesc, 0, 1);
            this.TableLayoutPanel1.Controls.Add(this.LabelFam, 0, 2);
            this.TableLayoutPanel1.Controls.Add(this.LabelSsFam, 0, 3);
            this.TableLayoutPanel1.Controls.Add(this.LabelMarq, 0, 4);
            this.TableLayoutPanel1.Controls.Add(this.LabelPrix, 0, 5);
            this.TableLayoutPanel1.Controls.Add(this.LabelQt, 0, 6);
            this.TableLayoutPanel1.Controls.Add(this.ComboBoxFam, 1, 2);
            this.TableLayoutPanel1.Controls.Add(this.ComboBoxSsFam, 1, 3);
            this.TableLayoutPanel1.Controls.Add(this.ComboBoxMarq, 1, 4);
            this.TableLayoutPanel1.Controls.Add(this.TextBoxRef, 1, 0);
            this.TableLayoutPanel1.Controls.Add(this.TextBoxDesc, 1, 1);
            this.TableLayoutPanel1.Controls.Add(this.NumericUpDownPrix, 1, 5);
            this.TableLayoutPanel1.Controls.Add(this.NumericUpDownQt, 1, 6);
            this.TableLayoutPanel1.Location = new System.Drawing.Point(-4, -1);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 7;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 14F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(487, 362);
            this.TableLayoutPanel1.TabIndex = 0;
            // 
            // LabelRef
            // 
            this.LabelRef.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelRef.AutoSize = true;
            this.LabelRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelRef.Location = new System.Drawing.Point(15, 17);
            this.LabelRef.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelRef.Name = "LabelRef";
            this.LabelRef.Size = new System.Drawing.Size(96, 20);
            this.LabelRef.TabIndex = 0;
            this.LabelRef.Text = "Référence :";
            // 
            // LabelDesc
            // 
            this.LabelDesc.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelDesc.AutoSize = true;
            this.LabelDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelDesc.Location = new System.Drawing.Point(15, 68);
            this.LabelDesc.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelDesc.Name = "LabelDesc";
            this.LabelDesc.Size = new System.Drawing.Size(105, 20);
            this.LabelDesc.TabIndex = 1;
            this.LabelDesc.Text = "Description :";
            // 
            // LabelFam
            // 
            this.LabelFam.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelFam.AutoSize = true;
            this.LabelFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelFam.Location = new System.Drawing.Point(15, 118);
            this.LabelFam.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelFam.Name = "LabelFam";
            this.LabelFam.Size = new System.Drawing.Size(73, 20);
            this.LabelFam.TabIndex = 2;
            this.LabelFam.Text = "Famille :";
            // 
            // LabelSsFam
            // 
            this.LabelSsFam.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelSsFam.AutoSize = true;
            this.LabelSsFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F);
            this.LabelSsFam.Location = new System.Drawing.Point(15, 170);
            this.LabelSsFam.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelSsFam.Name = "LabelSsFam";
            this.LabelSsFam.Size = new System.Drawing.Size(117, 20);
            this.LabelSsFam.TabIndex = 3;
            this.LabelSsFam.Text = "Sous-Famille :";
            // 
            // LabelMarq
            // 
            this.LabelMarq.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelMarq.AutoSize = true;
            this.LabelMarq.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMarq.Location = new System.Drawing.Point(15, 222);
            this.LabelMarq.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelMarq.Name = "LabelMarq";
            this.LabelMarq.Size = new System.Drawing.Size(75, 20);
            this.LabelMarq.TabIndex = 4;
            this.LabelMarq.Text = "Marque :";
            // 
            // LabelPrix
            // 
            this.LabelPrix.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelPrix.AutoSize = true;
            this.LabelPrix.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelPrix.Location = new System.Drawing.Point(15, 273);
            this.LabelPrix.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelPrix.Name = "LabelPrix";
            this.LabelPrix.Size = new System.Drawing.Size(71, 20);
            this.LabelPrix.TabIndex = 5;
            this.LabelPrix.Text = "PrixHT :";
            // 
            // LabelQt
            // 
            this.LabelQt.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelQt.AutoSize = true;
            this.LabelQt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelQt.Location = new System.Drawing.Point(15, 325);
            this.LabelQt.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelQt.Name = "LabelQt";
            this.LabelQt.Size = new System.Drawing.Size(87, 20);
            this.LabelQt.TabIndex = 6;
            this.LabelQt.Text = "Quantité : ";
            // 
            // ComboBoxFam
            // 
            this.ComboBoxFam.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ComboBoxFam.FormattingEnabled = true;
            this.ComboBoxFam.Location = new System.Drawing.Point(150, 116);
            this.ComboBoxFam.Name = "ComboBoxFam";
            this.ComboBoxFam.Size = new System.Drawing.Size(333, 24);
            this.ComboBoxFam.TabIndex = 9;
            // 
            // ComboBoxSsFam
            // 
            this.ComboBoxSsFam.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ComboBoxSsFam.FormattingEnabled = true;
            this.ComboBoxSsFam.Location = new System.Drawing.Point(150, 167);
            this.ComboBoxSsFam.Name = "ComboBoxSsFam";
            this.ComboBoxSsFam.Size = new System.Drawing.Size(333, 24);
            this.ComboBoxSsFam.TabIndex = 10;
            // 
            // ComboBoxMarq
            // 
            this.ComboBoxMarq.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ComboBoxMarq.FormattingEnabled = true;
            this.ComboBoxMarq.Location = new System.Drawing.Point(150, 219);
            this.ComboBoxMarq.Name = "ComboBoxMarq";
            this.ComboBoxMarq.Size = new System.Drawing.Size(333, 24);
            this.ComboBoxMarq.TabIndex = 11;
            // 
            // TextBoxRef
            // 
            this.TextBoxRef.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TextBoxRef.Location = new System.Drawing.Point(150, 16);
            this.TextBoxRef.Name = "TextBoxRef";
            this.TextBoxRef.Size = new System.Drawing.Size(333, 22);
            this.TextBoxRef.TabIndex = 7;
            // 
            // TextBoxDesc
            // 
            this.TextBoxDesc.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TextBoxDesc.Location = new System.Drawing.Point(150, 58);
            this.TextBoxDesc.Multiline = true;
            this.TextBoxDesc.Name = "TextBoxDesc";
            this.TextBoxDesc.Size = new System.Drawing.Size(333, 40);
            this.TextBoxDesc.TabIndex = 8;
            // 
            // NumericUpDownPrix
            // 
            this.NumericUpDownPrix.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.NumericUpDownPrix.Location = new System.Drawing.Point(150, 272);
            this.NumericUpDownPrix.Name = "NumericUpDownPrix";
            this.NumericUpDownPrix.Size = new System.Drawing.Size(333, 22);
            this.NumericUpDownPrix.TabIndex = 12;
            // 
            // NumericUpDownQt
            // 
            this.NumericUpDownQt.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.NumericUpDownQt.Location = new System.Drawing.Point(150, 324);
            this.NumericUpDownQt.Name = "NumericUpDownQt";
            this.NumericUpDownQt.Size = new System.Drawing.Size(333, 22);
            this.NumericUpDownQt.TabIndex = 13;
            // 
            // ButtonValider
            // 
            this.ButtonValider.Location = new System.Drawing.Point(323, 358);
            this.ButtonValider.Name = "ButtonValider";
            this.ButtonValider.Size = new System.Drawing.Size(160, 38);
            this.ButtonValider.TabIndex = 1;
            this.ButtonValider.Text = "Valider";
            this.ButtonValider.UseVisualStyleBackColor = true;
            // 
            // buttonValider
            // 
            this.buttonValider.Location = new System.Drawing.Point(323, 358);
            this.buttonValider.Name = "buttonValider";
            this.buttonValider.Size = new System.Drawing.Size(160, 38);
            this.buttonValider.TabIndex = 1;
            this.buttonValider.Text = "Valider";
            this.buttonValider.UseVisualStyleBackColor = true;
            this.buttonValider.Click += new System.EventHandler(this.buttonValider_Click);
            // 
            // FenetreArticle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 403);
            this.Controls.Add(this.ButtonValider);
            this.Controls.Add(this.TableLayoutPanel1);
            this.MaximumSize = new System.Drawing.Size(500, 450);
            this.MinimumSize = new System.Drawing.Size(500, 450);
            this.Name = "FenetreArticle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter / Modifer Article";
            this.TableLayoutPanel1.ResumeLayout(false);
            this.TableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPrix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownQt)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        private System.Windows.Forms.Label LabelRef;
        private System.Windows.Forms.Label LabelDesc;
        private System.Windows.Forms.Label LabelFam;
        private System.Windows.Forms.Label LabelSsFam;
        private System.Windows.Forms.Label LabelMarq;
        private System.Windows.Forms.Label LabelPrix;
        private System.Windows.Forms.Label LabelQt;
        private System.Windows.Forms.Button ButtonValider;
        private System.Windows.Forms.ComboBox ComboBoxFam;
        private System.Windows.Forms.ComboBox ComboBoxSsFam;
        private System.Windows.Forms.ComboBox ComboBoxMarq;
        private System.Windows.Forms.TextBox TextBoxRef;
        private System.Windows.Forms.TextBox TextBoxDesc;
        private System.Windows.Forms.NumericUpDown NumericUpDownPrix;
        private System.Windows.Forms.NumericUpDown NumericUpDownQt;
    }
}